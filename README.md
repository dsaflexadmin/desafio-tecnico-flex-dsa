# Desafio Técnico Flex DSA - Estágio

Parabéns! Você foi selecionado para a próxima etapa do processo seletivo. Aqui você executará um pequeno projeto para nós avaliarmos seus conhecimentos técnicos.

Você terá 4 horas para realizar esse desafio, mas já salientamos que você não é obrigado a concluir todo o desafio; estaremos avaliando os seus conhecimentos técnicos nas tecnologias.

## O Projeto

Temos uma API pronta e precisamos apenas de um sistema de cadastro de usuários para autenticação com o serviço. Esse sistema de cadastro deve coletar as seguintes informações básicas:

-   Nome;
-   Senha;
-   Endereço de e-mail;
-   Data de nascimento;
-   Data de registro;
-   Endereços residenciais (pode ser mais de um), contendo
    -   Estado;
    -   Município;
    -   Bairro;
    -   Logradouro;
    -   Número (pode ser `NULL`);
    -   Complemento (pode ser `NULL`) e
    -   CEP.

Além de **cadastrar** novos usuários, o serviço deverá também **listar** todos os usuários cadastrados, **deletar** registros e também **modificar** entradas existentes.

## Arquitetura

O serviço deverá ser feito na arquitetura **cliente-servidor** para Web.

Deverá ter uma implementação de [API RESTful](https://hackernoon.com/restful-api-designing-guidelines-the-best-practices-60e1d954e7c9) no _backend_ num modelo padrão [MVC](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller) usual e o _frontend_ se conectará à API, mostrando em sua interface a listagem de registros e cadastrando/modificando usuários em formulários.

Disponibilizamos um banco no num servidor MySQL para você fazer sua implementação. Ele pode ser acessado em http://analytics.flexcontact.com.br:3306/ (acesso web via [phpmyadmin](http://analytics.flexcontact.com.br:3333)) com as credenciais no arquivo `credenciais-mysql.txt` na sua área de trabalho.

Sugerimos um arranjo de tabelas como a da figura abaixo

![tabelas](res/sugestao-esquema-tabelas.png)

## Tecnologias

Abaixo listamos algumas das tecnologias que podem ser utilizadas. Fique livre para utilizar quaisquer bibliotecas externas em seu projeto.

-   O _backend_ **deve** ser escrito em Python em alguma das frameworks disponíveis no mercado (Flask, Django, etc);

-   O _frontend_ pode ser escrito em qualquer linguagem e em qualquer Framework, mas preferimos Angular;

-   Fornecemos o banco de dados MySQL, mas você poderá utilizar instâncias locais como por exemplo SQLite se assim desejar, mas **deverá ser alguma tecnologia com persistência de dados**;

-   Git: Você clonará esse repositório e trabalhará nele;

-   Linux: Todo o desafio será realizado numa máquina Linux disponibilizada.

## Avaliação

Depois de entregue, avaliaremos os seguintes pontos:

-   Mantenha boas práticas de programação:

    -   Consistência;
    -   Nomeação de métodos e variáveis;
    -   Estruturação da pasta de projeto;
    -   Limpeza e clareza;
    -   etc

-   Siga as diretrizes de APIs REST.

### Bônus

-   Trabalhe com ambientes virtuais e crie uma lista de dependências, tanto para o Python (`requirements.txt`) quanto para o frontend;
-   Escreva uma boa documentação, comente seu código;
-   Uso do Git com versionamento claro e objetivo e/ou bom esquema de branches;
-   Desenho responsível no _frontend_;
-   Escreva um `Dockerfile` para rápido _deployment_ de sua solução

Depois você poderá ser chamado para uma entrevista técnica onde você explicará o raciocínio que levou durante a execução do projeto.

# Boa sorte!
